package com.example.application.ui.panel;

import com.example.application.tools.PropertyUtil;
import com.example.application.ui.UiConsts;
import com.example.application.ui.component.MyIconButton;
import com.example.application.Demo1;

import javax.swing.*;
import java.awt.*;

/**
 * 工具栏面板
 *
 * @author Bob
 */
public class ToolBarPanel extends JPanel {

    private static final long serialVersionUID = 1L;

    private static MyIconButton buttonStatus;
    private static MyIconButton buttonStatus2;

    /**
     * 构造
     */
    public ToolBarPanel() {
        initialize();
        addButtion();
        addListener();
    }

    /**
     * 初始化
     */
    private void initialize() {
        Dimension preferredSize = new Dimension(48, UiConsts.MAIN_WINDOW_HEIGHT);
        this.setPreferredSize(preferredSize);
        this.setMaximumSize(preferredSize);
        this.setMinimumSize(preferredSize);
        this.setBackground(UiConsts.TOOL_BAR_BACK_COLOR);
        this.setLayout(new GridLayout(2, 1));
    }

    /**
     * 添加工具按钮
     */
    private void addButtion() {

        JPanel panelUp = new JPanel();
        panelUp.setBackground(UiConsts.TOOL_BAR_BACK_COLOR);
        panelUp.setLayout(new FlowLayout(-2, -2, -4));

        buttonStatus = new MyIconButton(UiConsts.ICON_STATUS_ENABLE, UiConsts.ICON_STATUS_ENABLE,
                UiConsts.ICON_STATUS, PropertyUtil.getProperty("ds.ui.status.title"));

        panelUp.add(buttonStatus);

        buttonStatus2 = new MyIconButton(UiConsts.ICON_STATUS_ENABLE, UiConsts.ICON_STATUS_ENABLE,
                UiConsts.ICON_STATUS, PropertyUtil.getProperty("ds.ui.status.title"));

        panelUp.add(buttonStatus2);

        this.add(panelUp);

    }

    /**
     * 为各按钮添加事件动作监听
     */
    private void addListener() {
        buttonStatus.addActionListener(e -> {

            buttonStatus.setIcon(UiConsts.ICON_STATUS_ENABLE);

            Demo1.mainPanelCenter.removeAll();
            StatusPanel.setContent();
            Demo1.mainPanelCenter.add(Demo1.statusPanel, BorderLayout.CENTER);

            Demo1.mainPanelCenter.updateUI();

        });

        buttonStatus2.addActionListener(e -> {

            buttonStatus.setIcon(UiConsts.ICON_STATUS_ENABLE);

            Demo1.mainPanelCenter.removeAll();
            StatusPanel.setContent();
            Demo1.mainPanelCenter.add(Demo1.statusPanel, BorderLayout.CENTER);

            Demo1.mainPanelCenter.updateUI();

        });
    }
}
