package com.example.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.example.application.ui.UiConsts;
import com.example.application.ui.panel.StatusPanel;
import com.example.application.ui.panel.ToolBarPanel;

import javax.swing.*;
import java.awt.*;

/**
 * @author: xiaoqing
 * @date: 2022/4/9 1:40 PM
 * @description: 主启动类
 **/
public class Demo1 {

    private static final Logger logger = LoggerFactory.getLogger(Demo1.class);


    private JFrame frame;

    public static JPanel mainPanelCenter;

    public static StatusPanel statusPanel;


    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                Demo1 window = new Demo1();
                window.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * 构造，创建APP
     */
    public Demo1() {
        initialize();
    }

    /**
     * 初始化frame内容
     */
    private void initialize() {
        logger.info("==================AppInitStart");
        // 设置系统默认样式
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        // 初始化主窗口
        frame = new JFrame();
        frame.setBounds(UiConsts.MAIN_WINDOW_X, UiConsts.MAIN_WINDOW_Y, UiConsts.MAIN_WINDOW_WIDTH,
                UiConsts.MAIN_WINDOW_HEIGHT);
        frame.setTitle(UiConsts.APP_NAME);
        frame.setIconImage(UiConsts.IMAGE_ICON);
        frame.setBackground(UiConsts.MAIN_BACK_COLOR);

        JPanel mainPanel = new JPanel(true);
        mainPanel.setBackground(Color.white);
        mainPanel.setLayout(new BorderLayout());

        ToolBarPanel toolbar = new ToolBarPanel();
        mainPanel.add(toolbar, BorderLayout.WEST);

        mainPanelCenter = new JPanel(true);
        mainPanelCenter.setLayout(new BorderLayout());

        statusPanel = new StatusPanel();
        mainPanelCenter.add(statusPanel, BorderLayout.CENTER);

        mainPanel.add(mainPanelCenter, BorderLayout.CENTER);

        frame.add(mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        logger.info("==================AppInitEnd");
    }
}
