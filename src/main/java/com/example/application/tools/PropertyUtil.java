package com.example.application.tools;

import com.example.application.ui.UiConsts;

import java.io.*;
import java.util.Properties;

/**
 * Created by zhouy on 2017/2/27.
 */
public class PropertyUtil {

    /**
     * properties路径
     */
    public final static String PATH_PROPERTY = UiConsts.CURRENT_DIR + File.separator + "config" + File.separator
            + "zh-cn.properties";

    /**
     * 获取property
     *
     * @param key
     * @return
     */
    public static String getProperty(String key) {
        Properties pps = new Properties();
        try {
            InputStream in = new BufferedInputStream(new FileInputStream(PATH_PROPERTY));
            pps.load(in);
            String value = pps.getProperty(key);
            return value;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
