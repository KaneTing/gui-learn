package com.example.application;

/**
 * @author: xiaoqing
 * @date: 2022/4/10 2:16 PM
 * @description: LineType
 **/
public class LineType {
    private String name = "直线";
    private int colorX = 255;
    private int colorY= 87;
    private int colorZ= 51;
    private float weight= 3;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getColorX() {
        return colorX;
    }

    public void setColorX(int colorX) {
        this.colorX = colorX;
    }

    public int getColorY() {
        return colorY;
    }

    public void setColorY(int colorY) {
        this.colorY = colorY;
    }

    public int getColorZ() {
        return colorZ;
    }

    public void setColorZ(int colorZ) {
        this.colorZ = colorZ;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
}
