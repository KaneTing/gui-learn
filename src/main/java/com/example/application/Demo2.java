package com.example.application;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;

/**
 * @author: xiaoqing
 * @date: 2022/4/9 4:34 PM
 * @description: App2
 **/
public class Demo2 extends JFrame {

    public Demo2() {
        JPanel panel = new JPanel();
        getContentPane().add(panel);
        setSize(450, 450);

        JButton button = new JButton("press");
        panel.add(button);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        Line2D lin = new Line2D.Float(100, 100, 250, 260);
        g2.draw(lin);
    }

    public static void main(String[] args) {
        Demo2 s = new Demo2();
        s.setVisible(true);
    }
}