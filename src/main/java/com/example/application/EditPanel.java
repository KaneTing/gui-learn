package com.example.application;

import javax.swing.*;
import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.GeneralPath;
import java.util.List;
import java.util.*;

/**
 * 设备连接管理界面中的画图面板。在面板上画线，可以选择直线，可以移动、拉伸、删除直线。
 * @author 崔耀强 20150507-20150822
 * @version 1.1
 */
public class EditPanel extends JLayeredPane{

    private static final long serialVersionUID = 1L;

    public JFrame frame;

    public  Set<Line> lines;
    private Line inLine;
    private Line selectedLine; // 选中的line
    private Line tmpSelectedLine; // 移动时临时的线
    private LineType linetype;
    //private InterfaceBusiness interfaceBussiness;
    //private LineBusiness lineBusiness;

    private int module=1;//所属模块
    private float range = 4f;//判断两个点是否离得很近 近的标准用range
    public  boolean lineFlag=true;
    private boolean cureFlag;
    private int pressIndex = 0;



    public EditPanel(JFrame frame) {

        this.frame = frame;

        lines = new HashSet<Line>();
        //interfaceBussiness=new InterfaceBusiness();
        //lineBusiness=new LineBusiness();

        MyMouseListener mouseListener = new MyMouseListener();
        addMouseListener(mouseListener);
        addMouseMotionListener(mouseListener);
        //this.add(new JLabel(new ImageIcon("img/-1.jpg")));
        this.repaint();
    }
    /**
     * 面板鼠标事件（画线）包括画线过程中的所有的鼠标事件 实现了MouseInputListener接口
     ** 4种状态：0无任何状态1选中直线2选中端点3开始画线
     * */
    class MyMouseListener implements MouseInputListener {

        private Point oldPoint;
        boolean isDragge;
        private Point p1;
        private Point p2;
        private Point p3;
        private  List<LineType> lineTypeList;
        private boolean middleFlag;
        /**
         * 解决鼠标焦点超出面板（线的第一个坐标）
         * */
        private void constructStartP(Line ln, int x, int y) {
            if (x > getWidth()) {
                ln.startX = getWidth();
            } else if (x < 0) {
                ln.startX = 0;
            } else {
                ln.startX = x;
            }
            if (y > getHeight()) {
                ln.startY = getHeight();
            } else if (y < 0) {
                ln.startY = 0;
            } else
                ln.startY = y;
            //表示是折线
            if((ln.getLineShape()==1)){
                if(isRange(new Point(ln.startX,ln.startY), new Point(ln.middleX,ln.middleY))){

                    ln.lineShape=0;
                }
            }


        }
        /**
         * 解决鼠标焦点超出面板（线的第二个坐标）
         * */
        private void constructEndP(Line ln, int x, int y) {
            //表示是折线
            if((ln.getLineShape()==1)){
                if(isRange(new Point(ln.endX,ln.endY), new Point(ln.middleX,ln.middleY))){
                    ln.lineShape=0;
                }
            }
            if (x > getWidth())
                ln.endX = getWidth();
            else if (x < 0)
                ln.endX = 0;
            else
                ln.endX = x;
            if (y > getHeight())
                ln.endY = getHeight();
            else if (y < 0)
                ln.endY = 0;
            else
                ln.endY = y;
        }
        /**
         * 解决鼠标焦点超出面板（线的中间坐标）
         * */
        private void constructMiddleP(Line ln, int x, int y) {

            if(ln.getLineShape()!=2){
                ln.middleX = ln.startX;
                ln.middleY = ln.endY;
            }else{
                if (x > getWidth())
                    ln.middleX = getWidth();
                else if (x < 0)
                    ln.middleX = 0;
                else
                    ln.middleX = x;
                if (y > getHeight())
                    ln.middleY = getHeight();
                else if (y < 0)
                    ln.middleY = 0;
                else
                    ln.middleY = y;
            }
        }

        @Override
        /**
         * 鼠标点击面板选择画线（直线、曲线）
         * */
        public void mouseClicked(MouseEvent e) {
            if(e.getButton()==3){
                JPopupMenu  popup = new JPopupMenu();
                JMenu lineMenu=new JMenu("画直线");
                JMenu arcMenu=new JMenu("画曲线");
                lineTypeList=new ArrayList <LineType>();
                try {
                    //lineTypeList=lineBusiness.getLineTypeList();

                } catch (Exception e2) {
                    throw new RuntimeException();
                }
                //直线
                for(LineType lineType:lineTypeList){
                    JMenuItem lineItem = new JMenuItem(lineType.getName());
                    lineMenu.add(lineItem);
                    lineMenu.addSeparator();
                    lineItem.addActionListener(new LineAction());
                }
                //曲线
                for(LineType lineType:lineTypeList){
                    JMenuItem arcItem = new JMenuItem(lineType.getName());
                    arcMenu.add(arcItem);
                    arcMenu.addSeparator();
                    arcItem.addActionListener(new ArcAction());
                }
                popup.add(lineMenu);
                popup.add(arcMenu);
                popup.show(e.getComponent(), e.getX(), e.getY());
            }
        }
        @Override
        public void mouseEntered(MouseEvent e) {
        }
        @Override
        public void mouseExited(MouseEvent e) {
        }
        @Override
        /**
         * 鼠标移动判断光标
         * */
        public void mouseMoved(MouseEvent e) {
            /** 判断是否移动到某直线上或者直线的端点上
             * setCursor为指定的光标设置光标图像。
             * **/
            frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));//  +
            if (pressIndex != 3)
                pressIndex = 0;
            // 先清空
            for (Line line : lines) {
                line.isSelectP0 = false;
                line.isSelectP1 = false;
                line.isSelectP2 = false;
                line.isSelected = false;
            }
            if (pressIndex == 3) {
                frame.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
                return;
            }
            // 判断是否选中直线，若是则将光标改变
            for (Line line : lines) {
                if (line.contains(e.getX(), e.getY())) {
                    middleFlag=false;
                    line.isSelected = true;
                    pressIndex = 1;
                    frame.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                    break;
                }
            }
            // 如果选中了某条直线，再判断该直线的端点是否被选中
            if (selectedLine != null) {
                middleFlag=false;
                Line line = selectedLine;
                Point p = new Point(e.getX(), e.getY());
                p1 = new Point(line.startX, line.startY); // 左端点
                p2 = new Point(line.endX, line.endY); // 右端点
                p3 = new Point(line.middleX, line.middleY); // 中间端点

                if (isRange(p1, p)) {
                    line.isSelectP0 = true;
                    pressIndex = 2;
                    frame.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
                }
                if (isRange(p2, p)) {
                    line.isSelectP1 = true;
                    pressIndex = 2;
                    frame.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
                }
                if(isRange(p3,p)){
                    line.isSelectP2 = true;
                    pressIndex = 2;
                    middleFlag=true;
                    frame.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
                }
            }
        }
        @Override
        /**
         * 鼠标摁下事件
         * */
        public void mousePressed(MouseEvent e) {
            switch (pressIndex) {
                case 0:
                    selectedLine = null;
                    for (Line line : lines) {
                        line.isSelectP0 = false;
                        line.isSelectP1 = false;
                        line.isSelectP2 = false;
                        line.isSelected = false;
                    }
                    break;
                case 1:
                    for (Line line : lines) {
                        if (line.isSelected) {
                            selectedLine = line;
                            break;
                        }
                    }
                    tmpSelectedLine = new Line();
                    tmpSelectedLine.setLineType(selectedLine.getLineType());
                    tmpSelectedLine.lineShape=selectedLine.lineShape;
                    tmpSelectedLine.startX = selectedLine.startX;
                    tmpSelectedLine.endX = selectedLine.endX;
                    tmpSelectedLine.startY = selectedLine.startY;
                    tmpSelectedLine.endY = selectedLine.endY;
                    tmpSelectedLine.middleX = selectedLine.middleX;
                    tmpSelectedLine.middleY = selectedLine.middleY;
                    tmpSelectedLine.constructShape( );
                    oldPoint = new Point(e.getX(), e.getY());

                    break;
                case 2:
                    break;
                case 3:
                    inLine = new Line();
                    inLine.startX = e.getX();
                    inLine.startY = e.getY();
                    inLine.setLineType(linetype);

                    break;
                default:
                    break;
            }
        }

        @Override
        /**
         * 鼠标拖动事件
         * */
        public void mouseDragged(MouseEvent e) {

            switch (pressIndex) {
                case 0:
                    break;
                case 1:
                    Point point = new Point(e.getX(), e.getY());
                    int lenx = point.x - oldPoint.x;
                    int leny = point.y - oldPoint.y;
                    tmpSelectedLine.startX = selectedLine.startX + lenx;
                    tmpSelectedLine.endX = selectedLine.endX + lenx;
                    tmpSelectedLine.startY = selectedLine.startY + leny;
                    tmpSelectedLine.endY = selectedLine.endY + leny;
                    tmpSelectedLine.middleX = selectedLine.middleX+ lenx;
                    tmpSelectedLine.middleY = selectedLine.middleY+ leny;
                    tmpSelectedLine.constructShape( );
                    isDragge = true;
                    break;
                case 2:
                    Line line = selectedLine;// lines.get(selectPointInWhichLine);
                    if (line.isSelectP0) {
                        constructStartP(line, e.getX(), e.getY());
                    }
                    if (line.isSelectP1) {
                        constructEndP(line, e.getX(), e.getY());
                    }
                    if (line.isSelectP2) {//2 曲线 1 折线 0 直线
                        if(!(line.getLineShape()==2)){
                            line.lineShape=1;
                        }
                        constructMiddleP(line, e.getX(), e.getY());
                    }
                    line.constructShape( );

                    break;
                case 3:
                    //判断是否超出面板
                    constructEndP(inLine, e.getX(), e.getY());
                    //对线的形状处理
                    inLine.constructShape( );
                    break;
                default:
                    break;
            }
            repaint();
        }

        @Override
        /**
         * 鼠标释放事件
         * */
        public void mouseReleased(MouseEvent e) {
            switch (pressIndex) {
                case 0:
                    break;
                case 1:
                    Point point = new Point(e.getX(), e.getY());
                    if(!middleFlag){
                        try {
                            int lenx = point.x - oldPoint.x;
                            int leny = point.y - oldPoint.y;
                            selectedLine.startX += lenx;
                            selectedLine.startY += leny;
                            selectedLine.endX += lenx;
                            selectedLine.endY += leny;
                            selectedLine.middleX += lenx;
                            selectedLine.middleY += leny;
                            selectedLine.constructShape( );

                            //lineBusiness.updateLine(selectedLine);
                        } catch (Exception e2) {
                            throw new RuntimeException();
                        }
                        tmpSelectedLine = null;
                        //返回此鼠标事件是否为该平台的弹出菜单触发事件
                        if (!isDragge && e.isPopupTrigger()) {
                            JPopupMenu  popup = new JPopupMenu();
                            JMenuItem removeItem = new JMenuItem("删除线条");
                            JMenuItem updateItem = new JMenuItem("修改线长");
                            popup.add(removeItem);
                            popup.addSeparator();
                            popup.add(updateItem);
                            removeItem.addActionListener(new RemoveAction());
                            updateItem.addActionListener(new UpdateAction());
                            popup.show(e.getComponent(), e.getX(), e.getY());
                        }
                        isDragge = false;
                    }else{
                        try {
                            selectedLine.lineShape=1;
                            selectedLine.startX = selectedLine.startX;
                            selectedLine.startY = selectedLine.startY;
                            selectedLine.endX = selectedLine.endX;
                            selectedLine.endY = selectedLine.endY;
                            selectedLine.middleX = e.getX();
                            selectedLine.middleY = e.getY();
                            selectedLine.constructShape( );

                            //lineBusiness.updateLine(selectedLine);
                        } catch (Exception e2) {
                            throw new RuntimeException();
                        }
                        tmpSelectedLine = null;
                        isDragge = false;
                    }
                    break;
                case 2:
                    try {
                        Line line = selectedLine;

                        if (line.isSelectP0) {
                            constructStartP(line, e.getX(), e.getY());
                        }
                        if (line.isSelectP1) {
                            constructEndP(line, e.getX(), e.getY());
                        }
                        if (line.isSelectP2) {
                            constructMiddleP(line, e.getX(), e.getY());
                        }
                        //lineBusiness.updateLine(line);
                        line.constructShape( );
                    } catch (Exception e2) {
                        throw new RuntimeException();
                    }
                    break;
                case 3:
                    if(cureFlag){
                        inLine.setLineShape(2);
                    }else{
                        inLine.setLineShape(0);
                    }
                    constructEndP(inLine, e.getX(), e.getY());
                    inLine.constructShape( );
                    //规定长度必须大于5
                    if(inLine.length>5){
//                        try {
//
//                            int id=lineBusiness.getMaxId();
//                            inLine.setId(id+1);
//                        } catch (Exception e2) {
//                            throw new RuntimeException();
//                        }
//                        LineLengthDialog  lineLength=new LineLengthDialog(frame,inLine);
//                        lineLength.setVisible(true);
//                        boolean ok_cancel=lineLength.getClickSource();
//                        if(ok_cancel){
//                            lines.add(inLine);
//                        /*selectedLine = inLine;
//                        selectedLine.isSelected = true;*/
//                        }
//                        inLine = null;
//                        pressIndex = 0;
//                        break;
                    }

                default:
                    break;
            }
            // 统一在鼠标释放的时候重绘
            repaint();
        }
    }
    /**
     * 删除线条类
     * */
    class RemoveAction extends AbstractAction {

        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e) {

            Object[] options = { "OK", "CANCEL" };
            int op=JOptionPane.showOptionDialog(null, "确定删除???", "Warning",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
                    null, options, options[0]);

            if(op==0){
                int id=selectedLine.getId();
                try {
                    try {
                        //lineBusiness.removeLine(id);
                    } catch (Exception e2) {
                        throw new RuntimeException();
                    }
                    if (lines.remove(selectedLine)){
                        selectedLine = null;
                        pressIndex = 0;
                    }
                } catch (Exception e2) {
                    JOptionPane.showMessageDialog(null, "数据库删除线条失败!!!");
                }
            }
            repaint();
        }
    }
    /**
     * 修改线条长度类
     * */
    class UpdateAction extends AbstractAction {

        private static final long serialVersionUID = 1L;
        private JTextPane jtp;
        private JTextField jtp2;
        private Line myLine2;
        @Override
        public void actionPerformed(ActionEvent e) {
            //线条不为空
            int id=selectedLine.getId();
            final JDialog dialog = new JDialog(frame);
            dialog.setTitle("修改线长度");
            dialog.setModal(true);
            dialog.setSize(260, 200);
            dialog.setLocationRelativeTo(frame);
            dialog.setLayout(null);
            JLabel lineIDLabel=new JLabel(" 线  条  ID:");
            lineIDLabel.setHorizontalAlignment(SwingConstants.RIGHT);
            lineIDLabel.setBounds(5, 10, 100, 30);
            JTextPane jtpId=new JTextPane();
            jtpId.setEditable(false);
            jtpId.setText(id+"");
            jtpId.setBounds(110, 11, 80, 20);
            JLabel beforeChangeLabel=new JLabel("修改前线条长度:");
            beforeChangeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
            beforeChangeLabel.setBounds(5, 40, 100, 30);
            jtp=new JTextPane();
            jtp.setEditable(false);
            jtp.setBounds(110, 45, 80, 20);
            try {
                //myLine2= lineBusiness.getLineByID(id);
                myLine2 = null;
            } catch (Exception e2) {
                throw new RuntimeException();
            }
            jtp.setText(myLine2.getDistance()+"");
            JLabel afterChangeLabel=new JLabel("修改后线条长度:");
            afterChangeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
            afterChangeLabel.setBounds(5, 70, 100, 30);
            jtp2=new JTextField();
            jtp2.setBounds(110, 75, 80, 20);
            JPanel jp=new JPanel();
            JButton confirm=new JButton("确定");
            JButton cancel=new JButton("取消");
            jp.add(confirm);
            jp.add(cancel);
            jp.setBounds(0, 110, 280, 80);
            dialog.add(jp);
            dialog.add(lineIDLabel);
            dialog.add(beforeChangeLabel);
            dialog.add(jtpId);
            dialog.add(jtp);
            dialog.add(afterChangeLabel);
            dialog.add(jtp2);
            jtp2.addKeyListener(new KeyListener() {
                private int count;
                @Override
                public void keyTyped(KeyEvent e) {
                    int keyChar = e.getKeyChar();
                    if(keyChar >= KeyEvent.VK_0 && keyChar <= KeyEvent.VK_9 || keyChar==KeyEvent.VK_PERIOD){
                        if(keyChar==KeyEvent.VK_PERIOD){
                            count++;
                            if(count>=2){
                                e.consume();
                            }
                        }
                    }else{ // 如果输入的不是数字则屏蔽输入
                        e.consume(); //关键，屏蔽掉非法输入
                    }
                }
                @Override
                public void keyReleased(KeyEvent arg0) {
                }
                @Override
                public void keyPressed(KeyEvent arg0) {
                }
            });
            confirm.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    try {

                        String dis=jtp2.getText();
                        if(dis!=null){
                            myLine2.setDistance(Double.parseDouble(dis));
                            //lineBusiness.updateLine(myLine2);
                            //changeLineLength(myLine2);
                            Iterator<Line> iterator= lines.iterator();
                            while(iterator.hasNext()){
                                Line line=iterator.next();
                                if(line.getId()==myLine2.getId()){
                                    line.setDistance(myLine2.getDistance());
                                    dialog.dispose();
                                    return;
                                }
                            }
                        }

                    } catch (Exception e2) {
                        try {
                            throw new Exception();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }}
            });

            cancel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent arg0) {
                    pressIndex=0;
                    dialog.dispose();
                }
            });
            dialog.setVisible(true);
        }
    }
    /**
     * 选择画直线的类
     * */
    class LineAction extends AbstractAction {

        private static final long serialVersionUID = 1L;
        @Override
        public void actionPerformed(ActionEvent e) {
            try {

                String lineTypeName=((JMenuItem)e.getSource()).getText();
                //LineType lineType= lineBusiness.getLineTypeByName(lineTypeName);
                LineType lineType = null;
                        pressIndex=3;
                cureFlag=false;
                setMyLineType(lineType);
            } catch (Exception e2) {
                throw new RuntimeException();
            }
        }
    }
    /**
     * 选择画曲线的类
     * */
    class ArcAction extends AbstractAction {
        private static final long serialVersionUID = 1L;
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String lineTypeName=((JMenuItem)e.getSource()).getText();
                //LineType lineType=  lineBusiness.getLineTypeByName(lineTypeName);
                LineType lineType = null;

                cureFlag=true;
                pressIndex=3;
                setMyLineType(lineType);
            } catch (Exception e2) {
                throw new RuntimeException();
            }

        }
    }
    /**
     * 判断两个点是否离得很近 近的标准用range来横来
     * @param a 第一个点坐标
     * @param b  第二点坐标
     * @return 离得近为真，否则为假
     */
    private boolean isRange(Point a, Point b) {
        if (Math.hypot(a.x - b.x, a.y - b.y) < range)//返回 sqrt(x2 +y2)，没有中间溢出或下溢。
            return true;
        return false;
    }

    // 画图的按钮的背景和标签
    @Override
    protected void paintComponent(Graphics g2) {
        super.paintComponents(g2);
        //增加网格背景
        ImageIcon icon=new ImageIcon("configuration/sourceRealTimeMonitor/realTimeMonitor_Image/equConn_img\\-1.jpg");
        g2.drawImage(icon.getImage(),0, 0,this);
        Graphics2D g = (Graphics2D) g2;
        //消除锯齿
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        //Paint 接口定义如何为 Graphics2D 操作生成颜色模式。
        Paint oldPaint = g.getPaint();
        /*
         * Stroke 接口允许 Graphics2D 对象获得一个 Shape，该 Shape 是指定 Shape 的装饰轮廓
         * ，或该轮廓的风格表示形式。描绘一个 Shape 如同使用一支具有适当大小和形状的画笔描绘其轮廓
         * */
        Stroke s = g.getStroke();
        //画线
        for (Line line : lines) {

            LineType lineType=  line.getLineType();
            g.setStroke(new BasicStroke(lineType.getWeight()));
            int colorX= lineType.getColorX();
            int colorY= lineType.getColorY();
            int colorZ= lineType.getColorZ();
            g.setPaint(new Color(colorX, colorY, colorZ));
            GeneralPath shape = line.shape;
            g.setPaint(new Color(colorX, colorY, colorZ));
            g.draw(shape);
            g.fill(shape);
            /*
             * 画线长
             * */
            g.setPaint(Color.yellow);
            g.drawString(line.getDistance()+"", line.middleX+10, line.middleY);
        }

        // 如果被选择，那么直线的两端应该会有两个bai点
        if (selectedLine != null) {
            Line line = selectedLine;
            int side = (int) line.getLineType().getWeight();
            if(side==0){
                side=4;
            }
            g.setPaint(Color.white);
            g.drawOval((int) (line.startX - side / 1.5), line.startY - side / 2, side, side+1);
            g.drawOval((int) (line.endX - side / 1.5), line.endY - side / 2, side, side+1);
            g.fillOval((int) (line.startX - side / 1.5), line.startY - side / 2, side, side+1);
            g.fillOval((int) (line.endX - side / 1.5), line.endY - side / 2, side, side+1);
            g.drawOval((int) (line.middleX - side / 1.5), line.middleY - side / 2, side, side+1);
            g.fillOval((int) (line.middleX - side / 1.5), line.middleY - side / 2, side, side+1);
        }
        if (tmpSelectedLine != null) {
            LineType lineType=  tmpSelectedLine.getLineType();
            g.setStroke(new BasicStroke(lineType.getWeight()));
            g.setPaint(Color.BLACK);
            GeneralPath shape = tmpSelectedLine.shape;
            g.fill(shape);
        }

        // 画正在产生的直线
        if (inLine != null) {
            LineType lineType=  inLine.getLineType();
            g.setStroke(new BasicStroke(lineType.getWeight()));
            int colorX= lineType.getColorX();
            int colorY= lineType.getColorY();
            int colorZ= lineType.getColorZ();
            g.setPaint(new Color(colorX, colorY, colorZ));
            GeneralPath shape = inLine.shape;
            if (shape != null)
                g.fill(shape);
        }
        g.setPaint(oldPaint);
        g.setStroke(s);
        g2.dispose();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        if(lineFlag){
            try {

                if(lines!=null&&lines.size()>0){
                    lines.clear();
                }
//                ArrayList<Equipment> equPosList = interfaceBussiness.getPostionEqu(EquConnectionEditView.allEquList,module);
//                List<Integer> lineIdList=new ArrayList<Integer>();
//                lineIdList=lineBusiness.getLineIdList(equPosList);
//                List<Line> lineList =new ArrayList<Line>();
//                lineList = lineBusiness.getLineList(lineIdList);
//                if(lineList!=null){
//                    for(int i=0;i<lineList.size();i++){
//
//                        Line line=new Line();
//                        line.setId(lineList.get(i).getId());
//                        line.setDistance(lineList.get(i).getDistance());
//                        line.setLineShape(lineList.get(i).getLineShape());
//                        line.setLen(lineList.get(i).getLen());
//                        line.setStartX(lineList.get(i).getStartX());
//                        line.setStartY(lineList.get(i).getStartY());
//                        line.setEndX(lineList.get(i).getEndX());
//                        line.setEndY(lineList.get(i).getEndY());
//                        line.setMiddleX(lineList.get(i).getMiddleX());
//                        line.setMiddleY(lineList.get(i).getMiddleY());
//                        line.setLineType(lineList.get(i).getLineType());
//                        line.constructShape( );
//                        lines.add(line);
//                        lineFlag=false;
//                    }
//                }
                Line line=new Line();
                //line.setId(lineList.get(i).getId());
                line.setDistance(190);
                line.setLineShape(0);
                line.setLen(3);
                line.setStartX(10);
                line.setStartY(10);
                line.setEndX(200);
                line.setEndY(200);
                //line.setMiddleX(150);
                //line.setMiddleY(150);
                line.setLineType(new LineType());
                line.constructShape( );
                lines.add(line);
                lineFlag=false;

            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
        repaint();
        paintComponents(g);
    }
    /**
     * 用于绘制新线时给线设置线类型对象
     * */
    public  void setMyLineType(LineType lineType){
        linetype=lineType;
    }
}